<?php

include_once 'Curl.php';
include_once 'DigCrypt.php';

class Util extends Model {

    private $DigCrypt;

    public function __construct() {
        parent::__construct();
        $this->DigCrypt = new DigCrypt();
    }

    /**
     * getIPaddress
     * @return type
     */
    public function getIp() {
        $cIP = getenv('REMOTE_ADDR');
        $cIP1 = getenv('HTTP_X_FORWARDED_FOR');
        $cIP2 = getenv('HTTP_CLIENT_IP');
        $cIP1 ? $cIP = $cIP1 : null;
        $cIP2 ? $cIP = $cIP2 : null;
        return $cIP;
    }

    /**
     * xssFilter
     * @todo function
     * @param type $str
     * @return type
     */
    public function xssFilter($str) {
        return $str;
    }

    public function getServerIP() {
        return gethostbyname($_SERVER["SERVER_NAME"]);
    }

    /**
     * 
     * @param type $timestamp
     * @return string
     */
    public function dateTimeFormat($timestamp) {
        $timestamp = strtotime($timestamp);
        $curTime = time();
        $space = $curTime - $timestamp;
        //1分钟
        if ($space < 60) {
            $string = "刚刚";
            return $string;
        } elseif ($space < 3600) { //一小时前
            $string = floor($space / 60) . "分钟前";
            return $string;
        }
        $curtimeArray = getdate($curTime);
        $timeArray = getDate($timestamp);
        if ($curtimeArray['year'] == $timeArray['year']) {
            if ($curtimeArray['yday'] == $timeArray['yday']) {
                $format = "%H:%M";
                $string = strftime($format, $timestamp);
                return "今天 {$string}";
            } elseif (($curtimeArray['yday'] - 1) == $timeArray['yday']) {
                $format = "%H:%M";
                $string = strftime($format, $timestamp);
                return "昨天 {$string}";
            } else {
                $string = sprintf("%d月%d日 %02d:%02d", $timeArray['mon'], $timeArray['mday'], $timeArray['hours'], $timeArray['minutes']);
                return $string;
            }
        }
        $string = sprintf("%d-%d-%d", $timeArray['year'], $timeArray['mon'], $timeArray['mday'], $timeArray['hours'], $timeArray['minutes']);
        return $string;
    }

    /**
     * 
     * @param string $ip
     * @return type
     */
    public function ipConvAddress($ip) {
        $json = file_get_contents('http://ip.taobao.com/service/getIpInfo.php?ip=' . $ip);
        $arr = json_decode($json);
        return $arr->data;
    }

    public function digEncrypt($nums) {
        return $this->DigCrypt->en($nums);
    }

    public function digDecrypt($code) {
        return $this->DigCrypt->de($code);
    }

    /**
     * 性别eng转换
     * @param type $sex
     * @return string
     */
    public function sexConv($sex) {
        $s = array('f' => '女', 'm' => '男');
        if (array_key_exists($sex, $s)) {
            return $s[$sex];
        } else {
            return '未知';
        }
    }

}
